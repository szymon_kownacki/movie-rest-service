insert into movies (slug, title, year, description) values ('the-matrix', 'The Matrix', 1999, 'A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.')
insert into movies_genres (movie_slug, genre) values ('the-matrix', 'SCIFI')
insert into movies_genres (movie_slug, genre) values ('the-matrix', 'ACTION')


insert into movies (slug, title, year, description) values ('the-hobbit-an-unexpected-journey', 'The Hobbit: An Unexpected Journey', 2012, 'A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug.')
insert into movies_genres (movie_slug, genre) values ('the-hobbit-an-unexpected-journey', 'FANTASY')

insert into movies (slug, title, year, description) values ('the-hobbit-the-desolation-of-smaug', 'The Hobbit: The Desolation of Smaug', 2013, 'The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring.')
insert into movies_genres (movie_slug, genre) values ('the-hobbit-the-desolation-of-smaug', 'FANTASY')

insert into movies (slug, title, year, description) values ('the-hobbit-the-battle-of-the-five-armies', 'The Hobbit: The Battle of the Five Armies', 2014, ' Bilbo and Company are forced to be embraced in a war against an armed flock of combatants and the terrifying Smaug from acquiring a kingdom of treasure and incinerating all of Middle-Earth.')
insert into movies_genres (movie_slug, genre) values ('the-hobbit-the-desolation-of-smaug', 'FANTASY')
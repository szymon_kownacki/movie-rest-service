package com.roche.rad.http.rs;

import com.roche.rad.domain.Movie;
import com.roche.rad.repo.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MovieRestService {

    private MovieRepository movieRepository;

    @Autowired
    public MovieRestService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @RequestMapping(value = "/api/movies/{slug}", method = RequestMethod.PUT)
    public Movie create(@RequestBody Movie movie, @PathVariable("slug") String slug) {
        return movieRepository.save(movie);
    }

    @RequestMapping(value = "/api/movies", method = RequestMethod.GET)
    public Page<Movie> findAll(Pageable p) {
        return movieRepository.findAll(p);
    }

    @RequestMapping(value = "/api/movies/{slug}", method = RequestMethod.GET)
    public ResponseEntity<Movie> findOne(@PathVariable("slug") String slug) {
        Movie movie = movieRepository.findOne(slug);
        if (movie != null) {
            return new ResponseEntity<Movie>(movie, HttpStatus.OK);
        } else {
            return new ResponseEntity<Movie>(HttpStatus.NOT_FOUND);
        }
    }
}

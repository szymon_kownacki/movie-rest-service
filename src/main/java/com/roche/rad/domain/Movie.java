package com.roche.rad.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "movies")
public class Movie implements Serializable {

    @Id
    @Column
    private String slug;

    @Column
    private String title;

    @Column
    private int year;

    @Column
    private String description;

    @ElementCollection(targetClass = Movie.Genre.class, fetch = FetchType.EAGER)
    @Enumerated(value = EnumType.STRING)
    @CollectionTable(name = "movies_genres")
    @Column(name = "genre")
    private List<Genre> genres;

    public Movie() {
    }

    public Movie(String slug, int year, String title, String description, List<Genre> genres) {
        this.slug = slug;
        this.year = year;
        this.title = title;
        this.description = description;
        this.genres = genres;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public String getDescription() {
        return description;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public String getSlug() {
        return slug;
    }

    public static enum Genre {
        COMEDY, ACTION, SCIFI, FANTASY, DRAMA
    }
}
